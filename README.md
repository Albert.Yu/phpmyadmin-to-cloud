使用phpmyadmin遠端操作MYSQL
===================

[網頁好讀版](https://www.albert-yu.com/2017/10/%E4%BD%BF%E7%94%A8phpmyadmin%E9%81%A0%E7%AB%AF%E6%93%8D%E4%BD%9Cmysql/)

[GitLab](https://gitlab.com/Albert.Yu/phpmyadmin-to-cloud)

前言
===

目前各家雲端PaaS層的Mysql服務上有個問題是不知如何使用phpmyadmin連線至Mysql而又有需要的人

其實針對這點phpmyadmin已經有很好的解決方式

我們可以將phpmyadmin修改讓他可以遠端連線其他伺服器

最大的好處可以分成三個部分

1.登入介面集中管理

2.所有登入即操作紀錄都有LOG可以查詢

3.從傳統虛擬主機移轉時不需要修改作業習慣


操作方式概述
==========

主要是針對內建範例的設定檔進行複製與修改的作業

目前版本針對 phpmyadmin 4.8.0.1 進行修改，該版本於 2018-04-19 發佈

操作流程
=======

首先要先瞭解到如果要客制化設定的話需產生config.inc.php，並且設定該檔案

該檔案範例檔案為 config.sample.inc.php

這是在您下載phpmyadmin就有的檔案

首先將config.sample.inc.php複製並重新命名成config.inc.php

然後打開config.inc.php尋找下列原始碼

![avatar](https://www.albert-yu.com/wp-content/uploads/2018/06/螢幕快照-2018-06-05-上午12.52.17.png)

如果只是需要單台主機連線就把　$cfg['Servers'][$i]['host'] = 'localhost';

中的localhost改成遠端主機的IP或URL

如果是在單一個phpmyadmin中要控制多個主機則複製下列黃框中的文字

![avatar](https://www.albert-yu.com/wp-content/uploads/2018/06/螢幕快照-2018-06-05-上午12.52.17-2.png)

進一步往下貼上

![avatar](https://www.albert-yu.com/wp-content/uploads/2018/06/螢幕快照-2018-06-05-上午12.59.13.png)

貼上之後就會顯示如

![avatar](https://www.albert-yu.com/wp-content/uploads/2017/10/%E8%9E%A2%E5%B9%95%E5%BF%AB%E7%85%A7-2017-10-06-%E4%B8%8B%E5%8D%883.15.43.png)

這時就可以用您的帳號密碼進行登入